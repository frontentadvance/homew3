window.addEventListener('load', function () {
    const nameTitle = document.querySelector('.name-title'),
        foodImg = document.querySelector('.food-img'),
        ingridients = document.querySelector('.ingridients'),
        price = document.querySelector('.card-product-price'),
        likeCounter = document.querySelector('.like-counter'),
        addBasked = document.querySelector('.button-card'),
        countAdd = document.querySelector('.count-add'),
        countDel = document.querySelector('.count-del'),
        countText = document.querySelector('.count-text');
    let count = 1    

    let menu = JSON.parse(localStorage.menu);
    let id = JSON.parse(localStorage.id);
    foodImg.setAttribute('src', menu[id - 1].productImageUrl)
    nameTitle.innerHTML = menu[id - 1].productName
    ingridients.innerHTML = menu[id - 1].ingredients
    price.innerHTML = `${menu[id-1].price} грн`
    function foo(){
        if(menu[id-1].price * count >= 0){
            addBasked.innerHTML = `В кошик ${menu[id-1].price * count} грн`
        } else console.log("Так тримати!")
        // addBasked.innerHTML = `В кошик ${menu[id-1].price * count} грн`
    }
   
    
    
    
    console.log(menu)
    console.log(id)

    like()

    function like() {
        let [...imgLike] = document.getElementsByClassName('card-product-like');
        // console.log(imgLike)
        imgLike.forEach(function (elem) {
            elem.addEventListener('click', function (event) {
                if (elem.getAttribute('src') == 'img/like-none.png') {
                    elem.setAttribute('src', 'img/like.png')
                    console.log(event)
                    const i = JSON.parse(localStorage.menu)
                    i[id - 1].like = 1
                    likeCounter.innerHTML = `Количество лайков ${i[id - 1].like}`
                    localStorage.menu = JSON.stringify(i)
                } else if (elem.getAttribute('src') == 'img/like.png') {
                    elem.setAttribute('src', 'img/like-none.png')
                    console.log(event)
                    const i = JSON.parse(localStorage.menu)
                    i[id - 1].like = 0
                    likeCounter.innerHTML = `Количество лайков ${i[id - 1].like}`
                    localStorage.menu = JSON.stringify(i)
                }
            })
        })
    }

    function countA(){
        countAdd.addEventListener('click', function(){
            count++
            countText.innerHTML = count
            foo()
        })
    }
    countA()

    function countD(){
        countDel.addEventListener('click', function(){
            count--
            countText.innerHTML = count
            foo()
        })
    }
    countD()

const btn = document.querySelector('.button-card')
btn.addEventListener('click', function(){
    const sectionCard = document.querySelector('.section-card')
    sectionCard.style.display = "none"
})














})