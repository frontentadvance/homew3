let xhr = new XMLHttpRequest();
xhr.open("GET", "script/data.json")
xhr.onreadystatechange = function () {
    let menu
    if (xhr.readyState == 4 && xhr.status == 200) {
        menu = JSON.parse(xhr.responseText)
        // console.log(menu)
        function saveBD(menu) {
            localStorage.menu = JSON.stringify(menu)
        }
        menu.forEach(function (elem) {
            // console.log(elem);
            elem.like = 0
            addCard(elem.productImageUrl, elem.productName, elem.ingredients, elem.price, elem.id)
            saveBD(menu)
        });
    }
}
xhr.send()

Element.prototype.setAttributes = function (attrs) {
    for (var idx in attrs) {
        if ((idx === 'styles' || idx === 'style') && typeof attrs[idx] === 'object') {
            for (var prop in attrs[idx]) {
                this.style[prop] = attrs[idx][prop];
            }
        } else if (idx === 'html') {
            this.innerHTML = attrs[idx];
        } else {
            this.setAttribute(idx, attrs[idx]);
        }
    }
};

function addCard(imgURL, nameCard, textCard, cardPrice, id) {
    const cardSection = document.querySelector('.main-menu'),
        article = document.createElement('article'),
        img = document.createElement('img'),
        a = document.createElement('a'),
        p = document.createElement('p'),
        div = document.createElement('div'),
        price = document.createElement('p'),
        btn = document.createElement('button'),
        like = document.createElement('img')
    article.setAttribute('class', 'card-product');
    article.setAttribute('id', `${id}`);
    cardSection.appendChild(article);

    img.setAttributes({
        'class': 'card-product-img',
        'src': imgURL,
        'alt': '',
    });

    a.setAttribute('class', 'card-product-name');
    a.textContent = nameCard

    p.setAttributes({
        'class': 'card-product-inf'
    });
    p.textContent = textCard

    div.setAttribute('class', 'card-product-flex')
    article.append(img, a, p, div)

    price.setAttribute('class', 'card-product-price')
    price.textContent = cardPrice += " грн";

    btn.setAttributes({
        'type': 'button',
        'class': 'card-product-btn'
    });
    btn.textContent = 'В корзину';

    like.setAttributes({
        'src': 'img/like-none.png',
        'class': 'card-product-like',
        'alt': ''
    })

    div.append(price, btn, like)

}

setTimeout(like, 200)

function like() {
    let [...imgLike] = document.getElementsByClassName('card-product-like');
    // console.log(imgLike)
    imgLike.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            if (elem.getAttribute('src') == 'img/like-none.png') {
                elem.setAttribute('src', 'img/like.png')
                // console.log(event.path[2].id)
                const i = JSON.parse(localStorage.menu)
                i[event.path[2].id - 1].like = 1
                localStorage.menu = JSON.stringify(i)
            } else if (elem.getAttribute('src') == 'img/like.png') {
                elem.setAttribute('src', 'img/like-none.png')
                const i = JSON.parse(localStorage.menu)
                i[event.path[2].id - 1].like = 0
                localStorage.menu = JSON.stringify(i)
            }
        })
    })
    addBasked()
    link()
}
let sumPrice = 0

function addNewPos(nameHavchik, priceHavchik, count = 1) {
    const productPos = document.querySelector('.product-posision'),
        productCount = document.createElement('div'),
        productName = document.createElement('p'),
        countAdd = document.createElement('button'),
        countText = document.createElement('span'),
        countDel = document.createElement('button'),
        countPrice = document.createElement('p');
    productPos.append(productCount);
    productCount.setAttribute('class', 'product-count');
    productName.setAttribute('class', 'product-name');
    productName.textContent = nameHavchik;
    countAdd.setAttributes({
        'class': 'count-add1 btn-count'
    });
    price = priceHavchik
    sumPrice += priceHavchik
    countAdd.textContent = '+'
    countText.setAttribute('class', 'count-text');
    countText.textContent = count
    countDel.setAttributes({
        'class': 'count-del1 btn-count'
    });
    countDel.textContent = '-'
    countPrice.setAttribute('class', 'count-price');
    countPrice.textContent = priceHavchik
    productCount.append(productName, countAdd, countText, countDel, countPrice);
    // counterAdd()
    order.innerHTML = `Замовити ${50 + sumPrice} грн`
    counter = 1
    myCounter()
}
let counter = 0
// счетчик для корзины 
function myCounter() {
    const [...aa] = document.querySelectorAll('.count-add1')

    aa.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            counter++
            event.path[1].childNodes[2].innerText = counter

            event.path[1].childNodes[4].innerText = sumPrice*counter

            order.innerHTML = `Замовити ${50 + sumPrice + sumPrice*counter - sumPrice} грн`
        })
    })

    const [...dd] = document.querySelectorAll('.count-del1')
    dd.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            counter--
            const productPos = event.path[1]

            if(counter <= 0) {
                deletePositionBasked(productPos, price)
            } else {
                event.path[1].childNodes[2].innerText = counter

                event.path[1].childNodes[4].innerText = price*counter
    
                order.innerHTML = `Замовити ${50 + price + price*counter - price} грн`
                counter = 1
            }
            
            
        })
        
    })
}

function deletePositionBasked(productPos, Price) {
    productPos.remove()
    order.innerHTML = `Замовити ${50 + Price + Price*counter - Price}`
    counter = 1

}


function addBasked() {
    const [...cardMass] = document.querySelectorAll('.card-product');
    // console.log(cardMass)
    cardMass.forEach(function (elem) {
        let bnt = elem.querySelector('.card-product-btn')
        bnt.addEventListener('click', function (event) {
            let menu = JSON.parse(localStorage.menu),
                tagPrice = menu[event.path[2].id - 1].price
            const nameHavchik = event.path[2].childNodes[1].innerText;
            addNewPos(nameHavchik, tagPrice)
        })
    })
}

function link() {
    const [...link] = document.querySelectorAll('.card-product-name')
    console.log(link)
    link.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            console.log(event.path[1].id - 1);
            const menu = JSON.parse(localStorage.menu)

            const block = document.querySelector('.section-card')
            const id = event.path[1].id
            foo3(id)
            
            block.style.display = "block"

            const exit = document.querySelector('.closed-btn');
            exit.addEventListener('click', function(){
                block.style.display = "none"
            })

            localStorage.menu = JSON.stringify(menu)
        })
    })
}





function foo3(id) {
    const nameTitle = document.querySelector('.name-title'),
        foodImg = document.querySelector('.food-img'),
        ingridients = document.querySelector('.ingridients'),
        price = document.querySelector('.card-product-price1'),
        likeCounter = document.querySelector('.like-counter'),
        addBasked = document.querySelector('.button-card'),
        countAdd = document.querySelector('.count-add'),
        countDel = document.querySelector('.count-del'),
        countText = document.querySelector('.count-text');
    let count = 1
    like1(id)
    let menu = JSON.parse(localStorage.menu);
    foodImg.setAttribute('src', menu[id - 1].productImageUrl)
    nameTitle.innerHTML = menu[id - 1].productName
    ingridients.innerHTML = menu[id - 1].ingredients
    price.innerHTML = `${menu[id-1].price} грн`
        

    function foo() {
        if (menu[id - 1].price * count >= 0) {
            addBasked.innerHTML = `В кошик ${menu[id-1].price * count} грн`
        } else console.log("Так тримати!")
        // addBasked.innerHTML = `В кошик ${menu[id-1].price * count} грн`

    }







    function like1(id) {
        let [...imgLike] = document.getElementsByClassName('card-product-like1');
        // console.log(imgLike)
        imgLike.forEach(function (elem) {
            elem.addEventListener('click', function (event) {
                if (elem.getAttribute('src') == 'img/like-none.png') {
                    elem.setAttribute('src', 'img/like.png')
                    console.log(event)
                    const menu = JSON.parse(localStorage.menu)
                    menu[id - 1].like = 1
                    likeCounter.innerHTML = `Количество лайков ${menu[id - 1].like}`
                    localStorage.menu = JSON.stringify(menu)
                } else if (elem.getAttribute('src') == 'img/like.png') {
                    elem.setAttribute('src', 'img/like-none.png')
                    console.log(event)
                    const menu = JSON.parse(localStorage.menu)
                    menu[id - 1].like = 0
                    likeCounter.innerHTML = `Количество лайков ${menu[id - 1].like}`
                    localStorage.menu = JSON.stringify(menu)
                }
            })
        })

    }

    function countA() {
        countAdd.addEventListener('click', function () {
            count++
            countText.innerHTML = count
            foo()

        })
    }
    countA()

    function countD() {
        countDel.addEventListener('click', function () {
            count--
            countText.innerHTML = count
            foo()
        })
    }
    countD()

    const btn = document.querySelector('.button-card')
    btn.addEventListener('click', function (event) {
        const sectionCard = document.querySelector('.section-card')
        sectionCard.style.display = "none"

        addNewPos(menu[id - 1].productName, menu[id - 1].price * count, count)
    })
}


const order = document.querySelector('.order-btn')
order